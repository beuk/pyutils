#!/usr/bin/python
from twisted.internet import reactor, task
import numpy as np
import pigpio
import warnings
import time
from rtttl import parse_rtttl


# Base GPIO class
class _Base(object):
    pi = None

    def __init__(self, pi, **kwargs):
        self.pi = pi


# Base class for outputs
class _Actor(_Base):
    pass


# Base class for inputs
class _Sensor(_Base):
    pass


class Output(_Actor):
    lc = None
    active_state = None
    state = None

    def __init__(self, pi, channel, active_state=pigpio.ON):
        super(_Actor, self).__init__(pi)
        self.channel = channel
        self.active_state = active_state
        self.pi.set_mode(self.channel, pigpio.OUTPUT)
        self.off()

    def on(self):
        self.cancel_loop()
        self.state = self.active_state
        self.pi.write(self.channel, self.state)

    def off(self):
        self.cancel_loop()
        self.state = int(not self.active_state)
        self.pi.write(self.channel, self.state)

    def toggle(self):
        self.state = int(not self.state)
        self.pi.write(self.channel, self.state)

    def loop(self, duration=0.5):
        self.cancel_loop()
        self.lc = task.LoopingCall(self.toggle)
        self.lc.start(duration)

    def cancel_loop(self):
        if self.lc and self.lc.running:
            self.lc.stop()


class Switch(_Sensor):
    callback = []
    loopingCall = []
    callLater = []
    
    def __init__(self, pi, channel, debounce=300, pull_up_down=pigpio.PUD_DOWN):
        super(_Sensor, self).__init__(pi)
        self.channel = channel
        self.pi.set_mode(self.channel, pigpio.INPUT)
        self.pi.set_pull_up_down(self.channel, pull_up_down)
        self.pi.set_glitch_filter(self.channel, debounce*1000)
        self.add_callback_cancel_loop()        

    def add_callback_cancel_loop(self):
        def cancel_loop(channel, level, tick):
            for lc in self.loopingCall:
                if lc.running:
                    lc.stop()
            for cl in self.callLater:
                if cl.active():
                    cl.cancel()
            self.loopingCall = []
            self.callLater = []
        self.pi.callback(self.channel, pigpio.EITHER_EDGE, cancel_loop)

    def add_callback(self, event, callback, repeat=None, start=1):
        def reset_callback(handle, delay):
            from twisted.internet.error import AlreadyCancelled, AlreadyCalled
            try:
                handle.reset(delay)
            except (AlreadyCancelled, AlreadyCalled):
                print('cannot reset')
                pass

        def callback_later_and_loop(channel, level, tick):
            if repeat is not None:
                lc = task.LoopingCall(callback, channel, level, tick)  
                self.loopingCall.append(lc)          
                cl = reactor.callLater(9999, lambda: lc.start(float(repeat)/1000))
            else:
                cl = reactor.callLater(9999, callback, channel, level, tick)
            self.callLater.append(cl)
            reactor.callFromThread(lambda: reset_callback(cl, float(start)/1000))
                             
        return self.pi.callback(self.channel, event, callback_later_and_loop)

    def read(self):
        return self.pi.read(self.channel)


class FlowSwitch(Switch):
    count = 0
    _reset = False

    def add_flow_callback(self, event, callback, amount=1, do_reset=True):
        """Adds callback to be fired after amount has been tallied"""
        def _callback(channel, level, tick):
            """Tally and execute callback if amount reached. Reset tally afterwards"""
            self.tally()
            if self.count >= amount:
                callback(channel, level, tick)
                if do_reset:
                    self.reset_tally()

        self.pi.callback(self.channel, event, _callback)

    def tally(self):
        """Increment the callback called count."""
        if self._reset:
            self._reset = False
            self.count = 0
        self.count += 1

    def reset_tally(self):
        """
        Resets the tally count to zero.
        """
        self._reset = True
        self.count = 0


class Servo(_Actor):
    position = 0
    pulse_width = None
    MIN = None
    MAX = None

    def __init__(self, pi, channel, max_width=2500, min_width=500):
        super(_Actor, self).__init__(pi)
        self.channel = channel
        self.pi.set_mode(self.channel, pigpio.OUTPUT)
        self.MAX = max_width
        self.MIN = min_width

    def set_pos(self, position):
        self.position = max(min(position, 100), 0)
        self.pulse_width = int(float(self.position) / 100 * (self.MAX - self.MIN) + self.MIN)
        self.pi.set_servo_pulsewidth(self.channel, self.pulse_width)

    def change_pos(self, increment):        
        self.set_pos(self.position + increment)


class Stepper(_Actor):
    speed_rps = 0
    ramp_pct = 0
    forward = True
    position = 0
    deg_per_step = 1.8
    mode = {1: (0, 0, 0),
            2: (1, 0, 0),
            4: (0, 1, 0),
            8: (1, 1, 0),
            16: (0, 0, 1),
            32: (1, 0, 1)}

    def __init__(self, pi, gpio_dir, gpio_step, gpio_mode, resolution=1, speed_rps=1, ramp_pct=0.5):
        super(_Actor, self).__init__(pi)
        self.gpio_dir = gpio_dir
        self.gpio_step = gpio_step
        self.gpio_mode = gpio_mode
        self.resolution = resolution
        self.speed_rps = speed_rps
        self.ramp_pct = ramp_pct
        self._set_pins()
        self._set_resolution(resolution)
        self.set_direction()

    def _set_pins(self):
        self.pi.set_mode(self.gpio_dir, pigpio.OUTPUT)
        self.pi.set_mode(self.gpio_step, pigpio.OUTPUT)

    def _set_resolution(self, key):
        for i in range(3):
            self.pi.write(self.gpio_mode[i], self.mode[key][i])

    def _steps_per_rev(self, rev=1):
        return int(rev * 360 / self.deg_per_step * self.resolution)

    def set_frequency(self, freq):
        self.pi.set_PWM_frequency(self.gpio_step, freq)

    def set_speed_rpm(self, rpm):
        self.set_frequency(200 /60 * rpm * self.resolution)

    def set_speed_hz(self, hz):
        self.set_frequency(200 * hz * self.resolution)

    def enable_pwm(self):
        self.pi.set_PWM_dutycycle(self.gpio_step, 128)  # PWM 1/2 On 1/2 Off

    def disable_pwm(self):
        self.pi.set_PWM_dutycycle(self.gpio_step, 0)  # PWM Off

    def set_direction(self, forward=True):
        self.pi.write(self.gpio_dir, int(forward))
        self.forward = forward

    def rotate_to_position(self, pos):
        revs = pos - self.position
        revs = revs if self.forward else -revs  # correct direction
        revs = revs if revs >= 0 else revs % 1  # never backwards
        self.rotate(revs, self.speed_rps,
                    revs_ramp_up=self.ramp_pct*revs,
                    revs_ramp_down=self.ramp_pct*revs)
        self.position = pos

    def rotate(self, revs, speed_rps, revs_ramp_up=0, revs_ramp_down=0):
        freq = speed_rps * self._steps_per_rev()
        count = revs * self._steps_per_rev()
        if count % 1 > 0:
            warnings.warn('Requested position no possible with current resolution, position will be rounded')
        ramp = [[int(freq), int(count)]]
        if revs_ramp_up > 0:
            ramp = self._add_ramp_up(ramp, self._steps_per_rev(revs_ramp_up))
        if revs_ramp_down > 0:
            ramp = self._add_ramp_down(ramp, self._steps_per_rev(revs_ramp_down))

        self.generate_ramp(ramp)
        self.position += revs * self.forward

    @staticmethod
    def _add_ramp_up(ramp_in, steps_total, levels=10):
        freq_st, count_st = ramp_in[0]
        freq = np.arange(start=int(freq_st / levels), stop=freq_st, step=int(freq_st / levels))
        count_per_hz = steps_total / sum(freq)
        count = [int(count_per_hz * f) for f in freq]
        ramp_up = np.column_stack((freq, count))
        return np.vstack((ramp_up, [[freq_st, count_st - sum(count)]]))

    @staticmethod
    def _add_ramp_down(ramp_in, steps_total, levels=10):
        freq_st, count_st = ramp_in[-1]
        freq = np.arange(start=freq_st, stop=0, step=-int(freq_st / levels))
        count_per_hz = steps_total / sum(freq)
        count = [int(count_per_hz * f) for f in freq]
        ramp_down = np.column_stack((freq, count))
        return np.vstack((ramp_in[:-1], [[freq_st, count_st - sum(count)]], ramp_down))

    def generate_ramp(self, ramp):
        # method taken directly from:
        # https://www.rototron.info/raspberry-pi-stepper-motor-tutorial/
        #
        """Generate ramp wave forms.
        ramp:  List of [Frequency, Steps]
        """
        self.pi.wave_clear()     # clear existing waves
        length = len(ramp)  # number of ramp levels
        wid = [-1] * length

        # Generate a wave per ramp level
        for i in range(length):
            frequency = ramp[i][0]
            micros = int(500000 / frequency)
            wf = []
            wf.append(pigpio.pulse(1 << self.gpio_step, 0, micros))  # pulse on
            wf.append(pigpio.pulse(0, 1 << self.gpio_step, micros))  # pulse off
            self.pi.wave_add_generic(wf)
            wid[i] = self.pi.wave_create()

        # Generate a chain of waves
        chain = []
        for i in range(length):
            steps = ramp[i][1]
            x = steps & 255
            y = steps >> 8
            chain += [255, 0, wid[i], 255, 1, x, y]

        self.pi.wave_chain(chain)  # Transmit chain.


class MCP3008(_Actor):
    cs = None

    def __init__(self, pi, cs, miso, mosi, sclk):
        super(_Actor, self).__init__(pi)
        self.cs = cs
        try:
            pi.bb_spi_close(cs)
        except pigpio.error:
            pass
        pi.bb_spi_open(cs, miso, mosi, sclk, 10000, 0)

    def get_voltage(self, channel):
        ct, data = self.pi.bb_spi_xfer(self.cs, [1, (8 + channel) << 4, 0])
        val = ((data[1] << 8) | data[2]) & 0x3FF
        return val / 1023 * 3.3


class Buzzer(_Actor):
    channel = []
    cl = []

    def __init__(self, pi, channels):
        super(_Actor, self).__init__(pi)
        self.channel = channels
        for channel in channels:
            self.pi.set_mode(channel, pigpio.OUTPUT)

    def playback_blocking(self, tune):
        print('play song')
        song = parse_rtttl(tune)
        for note in song['notes']:
            self.start_note(note['frequency'])
            time.sleep(note['duration']/1000)
        self.start_note(0)
        print("stop song\n")

    def playback(self, notes):
        self.stop_playback()
        duration = 0
        for note in notes:
            self.cl.append(reactor.callLater(duration, self.start_note, note['frequency']))
            duration += note['duration'] / 1000
        self.cl.append(reactor.callLater(duration, self.stop_note))

    def playback_rtttl(self, tune):
        song = parse_rtttl(tune)
        self.playback(song['notes'])

    def stop_playback(self):
        for call in self.cl:
            if call.active():
                call.cancel()
        self.cl = []
        self.stop_note()

    def start_note(self, tone):
        self.stop_note()
        if tone is 0:
            return

        frequency = int((1000 / tone) * 1000)
        gpio_on = 1 << self.channel[0]
        gpio_off = 1 << self.channel[1] if len(self.channel) > 1 else 0
        pulses = [pigpio.pulse(gpio_on, gpio_off, frequency),
                  pigpio.pulse(gpio_off, gpio_on, frequency)]
        self.pi.wave_add_generic(pulses)
        self.pi.wave_send_repeat(self.pi.wave_create())

    def stop_note(self):
        if self.pi.wave_tx_busy():
            wid = self.pi.wave_tx_at()
            self.pi.wave_tx_stop()
            self.pi.wave_delete(wid)
        self.pi.wave_clear()

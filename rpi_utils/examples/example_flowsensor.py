import time

import pigpio

from utils.gpio import FlowSensor

channel_sensor = 4
channel_pwm = 3
run_time = 60.0
sample_time = 100.0

pi = pigpio.pi()

# Setup flow sensor with callbacks
p = FlowSensor(pi, channel_sensor, pulses_per_liter=8000)
p.add_callback(1, lambda: print('callback na 1L'))
p.add_callback(5, lambda: print('callback na 5L'))

# Use PWM generator for testing
# (sensor 75Hz per L/min, flow around 10L/min)
pi.set_mode(channel_pwm, pigpio.OUTPUT)
pi.set_PWM_dutycycle(channel_pwm, 128)
pi.set_PWM_frequency(channel_pwm, 800)

start = time.time()
while (time.time() - start) < run_time:
    time.sleep(sample_time)
    flow = p.flow()
    print("Flow = {:0.3f} L/min".format(flow))

p.cancel()
pi.stop()

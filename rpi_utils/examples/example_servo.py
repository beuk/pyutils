from time import sleep

import pigpio
from numpy import arcsin, rad2deg

from utils.gpio import Servo

if __name__ == '__main__':
    # Setup pigpio deamon
    pi = pigpio.pi()  # exit script if no connection
    if not pi.connected:
        exit()

    # Init servo
    servo = Servo(pi, channel=3, min_width=750, max_width=2250)
    servo.set_pos(0)
    exit()

    # Loop
    val = 100
    while True:
        val = 0 if val is 200 else val+1
        if val <= 100:
            pos = rad2deg(arcsin(val/50-1))/1.8+50
            print(pos)
            servo.set_pos(pos)
            sleep(15/100)
        else:
            pos = rad2deg(arcsin(3-val/50))/1.8+50
            print(pos)
            servo.set_pos(pos)
            sleep(0.4/100)

    # Ask user input
    while True:
        try:
            val = int(input("Servo position: "))
        except ValueError:
            val = -1

        if (val >= 0) and (val <= 1):
            val = 100*val
            print('updating servo position to {}'.format(val))
            servo.set_pos(val)
        else:
            print('invalid')

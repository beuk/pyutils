from time import sleep

import pigpio

if __name__ == '__main__':
    # Setup pigpio deamon
    pi = pigpio.pi()  # exit script if no connection
    if not pi.connected:
        exit()

    # LEDS and RELAIS outputs
    CHANNEL_LED = [4, 15, 17, 18, 27]
    leds = [Output(pi, channel) for channel in CHANNEL_LED]
    
    Loop
    while True:
        pass
        [led.on() for led in leds]
        sleep(.1)
        [led.off() for led in leds]
        sleep(.1)

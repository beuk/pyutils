import pigpio
from twisted.internet import reactor
from PyUtils.GPIO import Buzzer

if __name__ == '__main__':
    # Setup pigpio deamon
    pi = pigpio.pi()  # exit script if no connection
    if not pi.connected:
        exit()

    buzzer = Buzzer(pi, 7)

    print('start')
    reactor.callLater(1, buzzer.playback_rtttl, 'reset:d=4,o=7,b=140:4g#')
    reactor.run()



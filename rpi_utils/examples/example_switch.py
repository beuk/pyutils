import time

import pigpio
from twisted.internet import reactor

from utils.gpio import Switch

if __name__ == '__main__':
    # Setup pigpio deamon
    pi = pigpio.pi()  # exit script if no connection
    if not pi.connected:
        exit()

    # Init switches and allow GPIOs to settle
    sw_up = Switch(pi, channel=3, debounce=100)
    sw_down = Switch(pi, channel=4, debounce=100)
    time.sleep(0.5)

    # Define callbacks, these should always have the function signature:
    #   f(channel, level, tick)
    def increment(channel, level, tick):
        global count
        count += 1
        print(count)
        return

    def decrement(channel, level, tick):
        global count
        count -= 1
        print(count)
        return

    # Add up down actions
    sw_down.add_callback(event=pigpio.FALLING_EDGE, callback=decrement, start=0)
    sw_down.add_callback(event=pigpio.FALLING_EDGE,callback=decrement, start=1000, repeat=100)
    sw_up.add_callback(event=pigpio.FALLING_EDGE, callback=increment, start=0)
    sw_up.add_callback(event=pigpio.FALLING_EDGE, callback=increment, start=1000, repeat=100)

    # Run reactor
    count = 0
    print('Starting reactor')
    reactor.run()
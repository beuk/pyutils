from time import sleep

import pigpio

from utils.gpio import Stepper

# def generate_ramp(ramp):
#     """Generate ramp wave forms.
#     ramp:  List of [Frequency, Steps]
#     """
#     pi.wave_clear()     # clear existing waves
#     length = len(ramp)  # number of ramp levels
#     wid = [-1] * length
#
#     # Generate a wave per ramp level
#     for i in range(length):
#         frequency = ramp[i][0]
#         micros = int(500000 / frequency)
#         wf = []
#         wf.append(pigpio.pulse(1 << STEP, 0, micros))  # pulse on
#         wf.append(pigpio.pulse(0, 1 << STEP, micros))  # pulse off
#         pi.wave_add_generic(wf)
#         wid[i] = pi.wave_create()
#
#     # Generate a chain of waves
#     chain = []
#     for i in range(length):
#         steps = ramp[i][1]
#         x = steps & 255
#         y = steps >> 8
#         chain += [255, 0, wid[i], 255, 1, x, y]
#
#     pi.wave_chain(chain)  # Transmit chain.


DIR = 20     # Direction GPIO Pin
STEP = 21    # Step GPIO Pin
SWITCH = 16  # GPIO pin of switch
MODE = (14, 15, 18)   # Microstep Resolution GPIO Pins

# Connect to pigpiod daemon
pi = pigpio.pi()
stepper = Stepper(pi, gpio_dir=DIR, gpio_mode=MODE, gpio_step=STEP, resolution=32, speed_rps=1, ramp_pct=.5)
try:
    while True:
        stepper.set_direction(forward=True)
        stepper.rotate_to_position(.25)
        sleep(2)
        stepper.rotate_to_position(.5)
        sleep(2)
        stepper.rotate_to_position(.75)
        sleep(2)
        stepper.rotate_to_position(1)
        sleep(2)
except KeyboardInterrupt:
    print("\nCtrl-C pressed.  Stopping PIGPIO and exiting...")
    stepper.disable_pwm()
    pi.stop()


# # Set up pins as an output
# pi.set_mode(DIR, pigpio.OUTPUT)
# pi.set_mode(STEP, pigpio.OUTPUT)
#
# # Set up input switch
# pi.set_mode(SWITCH, pigpio.INPUT)
# pi.set_pull_up_down(SWITCH, pigpio.PUD_UP)
#
# RESOLUTION = {'1/1': (0, 0, 0),
#               '1/2': (1, 0, 0),
#               '1/4': (0, 1, 0),
#               '1/8': (1, 1, 0),
#               '1/16': (0, 0, 1),
#               '1/32': (1, 0, 1)}
# for i in range(3):
#     pi.write(MODE[i], RESOLUTION['1/2'][i])
#
# try:
#     # Ramp up
#     # generate_ramp([[3200, 1000],
#     #                [5000, 1000],
#     #                [8000, 1000],
#     #                [10000, 1000],
#     #                [16000, 1000],
#     #                [20000, 10000]])
#
#     pi.set_PWM_dutycycle(STEP, 128)  # PWM 1/2 On 1/2 Off
#     pi.set_PWM_frequency(STEP, 400)  # 1Hz (60rpm) at 1/2 resolution
#     while True:
#         # # pi.write(DIR, pi.read(SWITCH))  # Set direction
#         sleep(.1)
#
# except KeyboardInterrupt:
#     print("\nCtrl-C pressed.  Stopping PIGPIO and exiting...")
# finally:
#     pi.set_PWM_dutycycle(STEP, 0)  # PWM off
#     pi.stop()

#
# # Set duty cycle and frequency
# pi.set_PWM_dutycycle(STEP, 128)  # PWM 1/2 On 1/2 Off
# pi.set_PWM_frequency(STEP, 2000)  # 500 pulses per second
#
# try:
#     while True:
#         pi.write(DIR, pi.read(SWITCH))  # Set direction
#         sleep(.1)
#
# except KeyboardInterrupt:
#     print ("\nCtrl-C pressed.  Stopping PIGPIO and exiting...")
# finally:
#     pi.set_PWM_dutycycle(STEP, 0)  # PWM off
#     pi.stop()






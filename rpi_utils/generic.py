import smtplib
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from time import time, mktime, strptime, strftime
import os
import inspect


def send_mail(send_from, send_to, subject, text, html_view='[text]', username='', password='', files=None,
              server="127.0.0.1"):
    assert isinstance(send_to, list)

    # Compose MIMEMultipart message
    msg = MIMEMultipart()
    msg['From'] = send_from
    msg['To'] = COMMASPACE.join(send_to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    # Compose and attach body text
    text_html = html_view.replace('[text]', text.replace('\n', '<br>').replace(' ', '&nbsp;'))
    msg.attach(MIMEText(text_html, 'html'))

    # Attach files to message
    for f in files or []:
        with open(f, "rb") as fil:
            part = MIMEApplication(
                fil.read(),
                Name=basename(f)
            )
        # After the file is closed
        part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
        msg.attach(part)

    # Send message
    smtp = smtplib.SMTP(server)
    smtp.ehlo()
    smtp.starttls()
    if username and password:
        smtp.login(username, password)
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.close()


def get_seconds_until(hour_of_day):
    now = time()
    next = mktime(strptime(strftime('%Y-%m-%d ') + hour_of_day, '%Y-%m-%d %H:%M'))
    if next < now:
        next += 24 * 3600
    return next - now


def get_environment():
    # TODO: extend to other non-pi systems?
    try:
        env = 'pi' if os.uname()[4].startswith("arm") else 'windows'
    except:
        env = 'windows'
    return env


def verbose_func(func):
    def func_wrapper(*args, **kwargs):
        name = func.__name__
        if name and name != 'func_wrapper' and name[0] is not '_':
            print('Calling: {}'.format(name))
        return func(*args, **kwargs)

    return func_wrapper


def verbose_class_funcs(cls):
    for name, method in inspect.getmembers(cls, inspect.isroutine):
        setattr(cls, name, verbose_func(method))
    return cls

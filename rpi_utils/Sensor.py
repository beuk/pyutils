from MAX6675.MAX6675 import MAX6675 as Parent6675
from Adafruit_MAX31855.MAX31855 import MAX31855 as Parent31855
import numpy as np
from collections import deque


class BaseSensor(object):
    pass


class TempSensor(BaseSensor):
    _N_RAW_VALUES = 10
    _LIM_HI = 400
    _LIM_LO = -10

    def __init__(self):
        self.offset = 0
        self._raw = deque([np.nan] * self._N_RAW_VALUES, maxlen=self._N_RAW_VALUES)

    @property
    def value(self):
        return np.mean(list(self._raw))

    def read_temp_raw(self):
        temp_raw = self.readTempC()
        if self._LIM_HI >= temp_raw >= self._LIM_LO:
            self._raw.append(temp_raw + self.offset)
        else:
            self._raw.append(np.nan)


class MAX6675(Parent6675, TempSensor):
    def __init__(self, **kwargs):
        TempSensor.__init__(self)
        Parent6675.__init__(self, **kwargs)


class MAX31855(Parent31855, TempSensor):
    def __init__(self, **kwargs):
        TempSensor.__init__(self)
        Parent31855.__init__(self, **kwargs)


def get_temp_sensor(sensor_type='', **kwargs):
    if sensor_type == 'MAX6675':
        return MAX6675(**kwargs)
    elif sensor_type == 'MAX31855':
        return MAX31855(**kwargs)
    else:
        raise NameError('invalid sensor type: {}'.format(type))


#!/usr/bin/python
import mysql.connector as mariadb


class BaseDb(object):
    pass


class MariaDb(BaseDb):
    conn = None
    cursor = None
    sessionId = None
    connected = False

    def __init__(self):
        pass

    def connect(self, host, port, db, user, password):
        try:
            # Try to connect to database and store connection and cursor and set flag
            self.conn = mariadb.connect(host=host, port=port, user=user, password=password, database=db)
            self.cursor = self.conn.cursor()
            self.connected = True
        except mariadb.Error as error:
            # Unable to connect, set flag and show message
            self.connected = False
            print("Cannot connect to database, error: {}".format(error))

    def query(self, query):
        # Return if no connection
        if not self.connected:
            pass

        # Execute and commit query
        self.cursor.execute(query)
        self.conn.commit()


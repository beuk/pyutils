from setuptools import setup

setup(name='rpi_utils',
      version='0.1',
      description='Various utilities for Raspberry Pi',
      url='https://bitbucket.org/beuk/pyutils',
      author='wbek',
      packages=['rpi_utils'],
      zip_safe=False)
